(function() {

    var BookApp = angular.module("BookApp", ["ui.router"]);

    var BookConfig = function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("overview", {
                url:"/overview",
                templateUrl:"views/overview.html",
                controller: "BooksCtrl as booksCtrl"
            })
            .state("reviews", {
                url:"/reviews/:title",
                templateUrl:"views/reviews.html",
                controller: "ReviewsCtrl as reviewsCtrl"
            })

            $urlRouterProvider.otherwise("/overview")
    }

    BookConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    var BookSvc = function($http, $q) {
        var bookSvc = this;

        bookSvc.getOverview = function(key) {
            var defer = $q.defer();
            var url = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
            $http.get(url, {
                params: {
                    'api-key':key
                }
            }).then(function(result) {
                var data = result.data.results;
                defer.resolve(data);
            }).catch(function(err) {
                defer.reject(err);
            })
            return(defer.promise);
        };
        
        // bookSvc.getReview = function(title, key) {
        //     var defer = $q.defer();
        //     var url = "https://api.nytimes.com/svc/books/v3/reviews."
        //     $http.get(url, url += '?', {
        //         params: {
        //             'title': title,                    
        //             'api-key': key
        //         }
        //     }).then(function(result) {
        //          var data = result.data.results;
        //         defer.resolve(data)
        //     }).catch(function(err) {
        //         defer.reject(err);
        //     })
        //     return(defer.promise);
        // };

    };

    BookSvc.$inject = ["$http", "$q"];


    var BooksCtrl = function($state, BookSvc) {
        var booksCtrl = this;
        booksCtrl.books = [];
        var key = "ab31a1ff89a34249b0567ad01ec17db6";

        BookSvc.getOverview(key)
            .then(function(result){
                console.log(">>>result in BookSvc.");
                console.log(result);
                booksCtrl.books = result;
            }).catch(function(err){
                console.error("Error in getOverview" ,err);
            })

        // BookSvc.getReview(title, key) {
        //     console.info(">>> book = ", title);
        //     $state.go("reviews", {  })
        // }

        // BookSvc.getReviews = function(title) {
        //     console.info(">>> book = ", title);
        // $state.go("reviews", { book: title })
        // }
    };

    BooksCtrl.$inject = ["$state", "BookSvc"];

    // var ReviewsCtrl = function ($stateParams, BookSvc){
    //     var reviewsCtrl = this;
    //     reviewsCtrl.title = $stateParams.title;
    //     reviewsCtrl.reviews = [];
    //     bookSvc.getReviews(reviewsCtrl.title, "")
    //     .then(function(review) {
    //         reviewsCtrl.review = review;
    //     })
    // };
    // ReviewsCtrl.$inject = ["$stateParams", "BookSvc"];

    BookApp.config(BookConfig);

    BookApp.service("BookSvc", BookSvc);
    BookApp.controller("BooksCtrl", BooksCtrl);
    // BookApp.controller("ReviewsCtrl", ReviewsCtrl);

}) ();